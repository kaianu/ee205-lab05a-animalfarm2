///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.hpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "fish.hpp"

using namespace std;


namespace animalfarm {

class Aku : public Fish {
public:
   double weight;

   Aku( double givenWeight, enum Color newColor, enum Gender newGender );

   void printInfo();
};

} // namespace animalfarm

