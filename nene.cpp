///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   03_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( string tagID, enum Color newColor, enum Gender newGender ) {
   gender = newGender;         /// Get from the constructor... not all cats are the same gender (this is a has-a relationship)
   species = "Loxioides bailleui";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
   featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
       /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period.
   tag = tagID;           /// A has-a relationship.  Every cat has its own name.
}


const string Nene::speak() {
   return string( "Nay, nay" );
}


/// Print our Cat and name first... then print whatever information Mammal holds.
void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tag << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm

